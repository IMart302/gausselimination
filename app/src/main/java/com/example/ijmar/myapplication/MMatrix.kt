package com.example.ijmar.myapplication

/**
 * Created by ijmar on 03/12/2017.
 */
class MMatrix {

    private var vals : Array<Float>? = null
    private var rows : Int = 0
    private var cols : Int = 0

    constructor(rows : Int, cols : Int){
        vals = Array(rows*cols, {0f})
        this.rows = rows
        this.cols = cols
    }

    constructor(rows : Int, cols : Int, values : Array<Float>){
        vals = Array<Float>(rows*cols, {0f})
        if(rows*cols == values.count()){
            var ref = vals
            if(ref != null){
                for (i in ref.indices){
                    ref[i] = values[i]
                }
            }
            this.rows = rows
            this.cols = cols
        }
    }

    fun setAt(x : Int, y : Int, v : Float) {
        var ref = vals
        if(ref != null) {
            val pos = y*this.cols + x
            if (pos < ref.count()){
                ref[pos] = v
            }
        }
    }

    fun rowCount() : Int {
        return this.rows
    }

    fun colCount() : Int {
        return  this.cols
    }

    fun getAt(x : Int, y : Int) : Float {
        var ref = vals
        if(ref != null) {
            val pos = y*this.cols + x
            if (pos < ref.count()){
                return ref[pos]
            }
        }
        return 0f
    }

    fun divideRow(row : Int, divisor : Float){
        if(row < this.rows){
            for(i in 0 .. (this.cols - 1)){
                setAt(i, row, getAt(i, row)/divisor)
            }
        }
    }

    fun elementalOp(row : Int , row2: Int , mul : Float){ // row -> row - mul*row2
        if(row < this.rows && row2 < this.rows){
            for(i in 0 .. (this.cols - 1)){
                this.setAt(i, row, this.getAt(i, row) - mul*this.getAt(i, row2))
            }
        }
    }

    companion object {
        fun GaussElimination(mat : MMatrix) : MMatrix? {
            if(mat.colCount() <= mat.rowCount()){
                return  null
            }
            var ret = MMatrix(mat.rowCount(), mat.colCount())
            for (i in 0 .. (mat.rowCount()-1)){
                for (j in 0 .. (mat.colCount()-1)){
                    ret.setAt(j, i, mat.getAt(j, i))
                }
            }

            for(i in 0 .. (mat.rowCount()-2)){
                if(ret.getAt(i, i) == 0f){
                    return null
                }
                ret.divideRow(i, ret.getAt(i, i))
                for (j in (i+1) .. (ret.rowCount()-1)){
                    var mul = ret.getAt(i, j)
                    ret.elementalOp(j, i, mul)
                }
            }

            ret.divideRow(ret.rowCount()-1, ret.getAt((ret.colCount() - 2), ret.rowCount()-1))
            for (i in (ret.rowCount()-1) downTo 1){
                if(ret.getAt(i, i) == 0f){
                    return null
                }
                ret.divideRow(i, ret.getAt(i, i))
                for (j in (i - 1) downTo 0){
                    var mul = ret.getAt(i, j)
                    ret.elementalOp(j, i, mul)
                }
            }
            //ret.divideRow(0, ret.getAt(ret.colCount()-2, 0))
            return ret
        }


        fun SystemIncosistent(mat : MMatrix) : Boolean {

            var ceros = false
            for(i in 0 .. (mat.rowCount()-1)){
                ceros = true
                for (j in 0 .. (mat.colCount()-1)){
                    if(mat.getAt(j, i).isNaN()){
                        return true
                    }
                    if(mat.getAt(j, i) != 0f){
                        ceros = false
                    }
                }
                if(ceros){
                    return true
                }
            }
            return false
        }
    }
}
package com.example.ijmar.myapplication

import android.content.DialogInterface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.AdapterView
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var cell_adapter : CellGridAdapter = CellGridAdapter(null, 8, 7, 5)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        cell_adapter = CellGridAdapter(this, 8, 7, 3)
        matrixshow.adapter = cell_adapter
        matrixshow.onItemClickListener = AdapterView.OnItemClickListener({parent, view, position, id ->
            cell_adapter.setSelected(position)
            matrixshow.adapter = cell_adapter
            matrixshow.invalidate()
        })

        class SeekBarImplement : SeekBar.OnSeekBarChangeListener{
            constructor()

            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                cell_adapter.setNumOfVars(p1 + 1)
                matrixshow.adapter = cell_adapter
                matrixshow.invalidate()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }
        }

        matvar.setOnSeekBarChangeListener(SeekBarImplement())

        one.setOnClickListener({
            changeMatView(1, 1)
            matrixshow.adapter = cell_adapter
            matrixshow.invalidate()
        })

        two.setOnClickListener({
            changeMatView(2, 1)
            matrixshow.adapter = cell_adapter
            matrixshow.invalidate()
        })

        three.setOnClickListener({
            changeMatView(3, 1)
            matrixshow.adapter = cell_adapter
            matrixshow.invalidate()

        })

        four.setOnClickListener({
            changeMatView(4, 1)
            matrixshow.adapter = cell_adapter
            matrixshow.invalidate()
        })

        five.setOnClickListener({
            changeMatView(5, 1)
            matrixshow.adapter = cell_adapter
            matrixshow.invalidate()
        })

        six.setOnClickListener({
            changeMatView(6, 1)
            matrixshow.adapter = cell_adapter
            matrixshow.invalidate()
        })

        seven.setOnClickListener({
            changeMatView(7, 1)
            matrixshow.adapter = cell_adapter
            matrixshow.invalidate()
        })

        eight.setOnClickListener({
            changeMatView(8, 1)
            matrixshow.adapter = cell_adapter
            matrixshow.invalidate()
        })

        nine.setOnClickListener({
            changeMatView(9, 1)
            matrixshow.adapter = cell_adapter
            matrixshow.invalidate()
        })

        zero.setOnClickListener({
            changeMatView(0, 1)
            matrixshow.adapter = cell_adapter
            matrixshow.invalidate()
        })

        dot.setOnClickListener({
            changeMatView(0, 2)
            matrixshow.adapter = cell_adapter
            matrixshow.invalidate()
        })

        minus.setOnClickListener({
            changeMatView(0, 3)
            matrixshow.adapter = cell_adapter
            matrixshow.invalidate()
        })

        backspace.setOnClickListener({
            changeMatView(0, 4)
            matrixshow.adapter = cell_adapter
            matrixshow.invalidate()
        })

        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setTitle("Mensaje")
        alertDialog.setMessage("Sistema Inconsistente")
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Entendido",
                DialogInterface.OnClickListener( {dialog, which -> dialog.dismiss()}
                )
        )

        calc.setOnClickListener({
            var m = cell_adapter.popMatrix()
            var g = MMatrix.GaussElimination(m)
            if(g != null){
                if(MMatrix.SystemIncosistent(g)){
                    alertDialog.show()
                }
                else {
                    cell_adapter.pushMatrix(g)
                    matrixshow.adapter = cell_adapter
                    matrixshow.invalidate()
                }
            }
        })
    }

    private fun changeMatView(value : Int, option : Int){
        var temp : String = cell_adapter.getSelectedVal()
        when(option){
            1 -> { // 0 -> 9
                if(temp == "0")
                    temp = value.toString()
                else
                    temp += value.toString()
                cell_adapter.setValueSelected(temp)
            }
            2 -> { // dot
                if(!temp.contains(".")){
                    temp += "."
                    cell_adapter.setValueSelected(temp)
                }
            }
            3 -> { // minus
                if(temp.contains("-")){
                    temp = temp.drop(1)
                    cell_adapter.setValueSelected(temp)
                }
                else{
                    if(temp.isNotEmpty() && temp != "0"){
                        temp = "-" + temp
                        cell_adapter.setValueSelected(temp)
                    }
                }
            }
            4 -> { // backspace
                if(temp.isNotEmpty()){
                    temp = temp.dropLast(1)
                    if(temp.isEmpty()){
                        cell_adapter.setValueSelected("0")
                    }
                    else{
                        cell_adapter.setValueSelected(temp)
                    }
                }
            }
        }
    }
}

package com.example.ijmar.myapplication


import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView


/**
 * Created by ijmar on 02/12/2017.
 */
class CellGridAdapter : BaseAdapter{


    private var mctx : Context? = null
    private var cells : Array<String> = arrayOf("0") // must be 7*8, because the b vector
    private var dimx : Int = 8 // 7 of matrix and one of b
    private var dimy : Int = 7
    private var vars : Int = 1 // number of actual variables
    private var select : Int = 0 // cell id selected

    constructor(ctx : Context?, dimx : Int , dimy : Int, vars : Int){
        this.mctx = ctx
        this.cells = Array(dimx*dimy, {"0"})
        this.dimx = dimx
        this.dimy = dimy
        this.vars = vars
        for (i in this.cells.indices){
            cells[i] = "0"
        }
    }

    fun setNumOfVars(vars : Int) {
        if(vars < 8){
            this.vars = vars
            var x : Int = this.select % this.dimx
            var y : Int = this.select / this.dimx
            if(x >= (vars + 1)){
                x = vars
            }
            if(y >= vars){
                y = vars - 1
            }
            var pos : Int = y*this.dimx + x
            setSelected(pos)
        }
    }

    fun setAtXY(x : Int, y : Int, v : String){
        var pos : Int = y*this.dimx + x
        if(pos < this.cells.count()){
            cells[pos] = v
        }
    }

    fun setSelected(pos : Int){
        var x : Int = pos % dimx
        var y : Int = pos / dimx
        if(x < (vars + 1) && y < vars) {
            select = pos
        }
    }

    fun getSelectedVal() : String{
        return cells[select]
    }

    fun setValueSelected(value : String) {
        this.cells[select] = value
    }

    fun popMatrix() : MMatrix {
        var ref = cells
        var mat = MMatrix(vars, vars+1)
        if(ref != null){
            var x = 0
            var y = 0
            var matx = 0
            var maty = 0
            var f = 0f
            for(i in ref.indices){
                x = i % dimx
                y = i / dimx
                if(x < (vars + 1) && y < vars){
                    if(ref[i].isNotEmpty()){
                        f = ref[i].toFloat()
                    }
                    else{
                        f = 0f
                    }
                    mat.setAt(matx, maty, f)
                    matx = (matx+1) % mat.colCount()
                    if(matx == 0){
                        maty = (maty + 1) % mat.rowCount()
                    }
                }
            }
        }
        return mat
    }

    fun pushMatrix(mat : MMatrix){
        for (x in 0 .. (mat.colCount()-1)){
            for (y in 0 .. (mat.rowCount()-1)){
                this.setAtXY(x, y, mat.getAt(x, y).toString())
            }
        }
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return cells.count()
    }

    override fun getItem(p0: Int): Any? {
       return null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var text = TextView(mctx)
        text.gravity = TextView.TEXT_ALIGNMENT_CENTER
        text.text = cells[position].toString()

        var x : Int = position % dimx
        var y : Int = position / dimx

        val c = this.mctx
        if(x >= vars){
            if(x == vars){
                if(c != null) {
                    text.setBackgroundColor(c.resources.getColor(R.color.colorPrimaryDark))
                }
            }
            else{
                text.text = ""
                if(c != null) {
                    text.setBackgroundColor(c.resources.getColor(R.color.hidden))
                }
            }
        }

        if(y >= vars){
            text.text = ""
            if(c != null) {
                text.setBackgroundColor(c.resources.getColor(R.color.hidden))
            }
        }

        if(position == select) {
            if (c != null) {
                text.setBackgroundColor(c.resources.getColor(R.color.matSelectCell))
            }
        }
        else{
            if(x < vars && y < vars){
                if (c != null) {
                    text.setBackgroundColor(c.resources.getColor(R.color.matUnselectCell))
                }
            }
        }
        return text
    }

}